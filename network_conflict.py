#!/usr/bin/env python3
#----------------------------------------------------
#  Определения конфликтов IP адресации
#       Версия: 2020-11-29
#----------------------------------------------------
'''
Исходные данные принимаются с stdin

Данные разделяются табуляцией
Пример исходных данных:

Branch_C  192.0.2.0/24
    192.0.2.8/30
Branch_A    192.0.2.10/32
    ...

'''
from collections import OrderedDict
from ipaddress import ip_network
import sys

if __name__ == '__main__':
    BRANCHS = OrderedDict()
    ERRORS = dict()
    BR = None
    for _line in sys.stdin.readlines():
        if _line.strip() == '': continue
        else:
            if not '/' in _line.strip(): raise Exception("Не найдена маска сети в строке \"{}\"".format(_line.strip()))
            _s = _line.strip().split('\t')
        if len(_s) == 2:
            BR = _s[0]
            if not BRANCHS.get(BR): BRANCHS[BR] = [_s[1]]
            else: raise Exception("Повторение бранча {}".format(BR))
        elif len(_s) == 1:
            if not BR: raise Exception("Не найдено имя бенча")
            BRANCHS[BR].append(_s[0])
        else: raise Exception("Не удалось распарсить исходные данные")

    Conflicts = False
    # ----------------------------------------------------------------------
    def show(_a, _b, _c, _d):
        global Conflicts
        if not Conflicts:
            print("{:^104}\n".format("Конфликты:"), "-" * 104, "\n|{:^25}|{:^21}| <==> |{:^21}|{:^25}|\n".format(
                "Площадка", "Сеть", "Сеть", "Площадка"), "-" * 104, "\n", end="", sep="")
            Conflicts = True
        print("|{:^25}|{:^21}| <==> |{:^21}|{:^25}|".format(_a, _b, _c , _d))
    # ----------------------------------------------------------------------
    while len(BRANCHS):
        _b = BRANCHS.popitem()
        while len(_b[1]):
            _n = _b[1].pop()
            try:
                net = ip_network(_n)
            except ValueError:
                if ERRORS.get(_b[0]): ERRORS[_b[0]].add(_n)
                else: ERRORS[_b[0]] = {_n}
                continue

            for _l in _b[1]:
                try:
                    _n2 = ip_network(_l)
                except ValueError:
                    if ERRORS.get(_b[0]): ERRORS[_b[0]].add(_l)
                    else: ERRORS[_b[0]] = {_l}
                    continue

                if net.overlaps(_n2): show(_b[0],_n,_l,_b[0]) # Проверка конфликта в рамках одного бренча

            for _i in BRANCHS.keys():
                for _j in BRANCHS[_i]:
                    try:
                        _n2 = ip_network(_j)
                    except ValueError:
                        if ERRORS.get(_i): ERRORS[_i].add(_j)
                        else: ERRORS[_i] = {_j}
                        continue
                    if net.overlaps(ip_network(_j)): # Проверка конфликта с остальными бренчами
                        show(_b[0], _n, _j, _i)

    if Conflicts: print("-"*104)
    else: print("Конфиликты не найдены")

    if len(ERRORS):
        print("\n"*3, "Ошибки исходных данных:")
        for i in ERRORS.keys(): print("{} => {}".format(i,ERRORS[i]))

